
from collections import defaultdict
import numpy as np
import os
import sys

from sklearn import svm
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.preprocessing import LabelBinarizer

from utils import average_precision


DESC_SIZE = {
    'HOG':  96,
    'HOF': 108,
    'MBH': 192,
}


experiments = {
    'small': {'K': 50},
    'big': {'K': 1000},
}


class Hollywood2(object):
    def __init__(self, K):
        basepath = os.path.join('data', 'hollywood2')
        feature_path = os.path.join(
            basepath, 'features', 'dense5.track15mbh')

        self.descriptor_type = 'MBH'
        self.models = ['FV', 'SFV']
        self.spms = [(1, 1, 1), (1, 1, 2), (1, 3, 1)]

        self.filelists = {
            'train': os.path.join(basepath, 'filelists', 'train.list'),
            'test': os.path.join(basepath, 'filelists', 'test.list'),
        }

        self.descriptor_base_path = os.path.join(feature_path, 'descs')
        self.subset_path = os.path.join(feature_path, 'subset.dat')
        self.subset_size = int(2e5)

        self.nr_pca_components = 64
        self.nr_clusters = K

        self.pca_path = os.path.join(feature_path, 'pca', 'pca_%d' % self.nr_pca_components)
        self.gmm_path = os.path.join(feature_path, 'gmm', 'gmm_%d' % self.nr_clusters)

        self.generic_video_path = os.path.join(basepath, 'videos', '%s.avi')
        self.generic_feature_path = os.path.join(
            feature_path, 'statistics_k_%d' % K, '%s', 'spm_%s', '%s.dat')

    def _spm2str(self, spm):
        return '%d%d%d' % spm

    def get_samples(self, splits=['train', 'test']):
        samples = []
        for split in splits:
            with open(self.filelists[split], 'r') as ff:
                samples += [line.split('-')[0] for line in ff.readlines()]
        return list(set(samples))

    def get_samples_with_labels(self, splits=['train', 'test']):
        samples_to_labels = defaultdict(list)
        for split in splits:
            with open(self.filelists[split], 'r') as ff:
                for line in ff.readlines():
                    sample = line.split('-')[0]
                    label = line.split(' ')[1]
                    samples_to_labels[sample].append(label)
        return samples_to_labels

    def get_descriptor_path(self, sample):
        return os.path.join(self.descriptor_base_path, "%s.dat" % sample)

    def get_feature_path(self, sample, model, spm):
        return self.generic_feature_path % (
            model.lower(), self._spm2str(spm), sample)

    def get_video_path(self, sample):
        return self.generic_video_path % sample

    def get_classifier(self):
        return Classifier()

    def load_subset(self):
        subset = np.fromfile(self.subset_path, dtype=np.float32)
        return subset.reshape(-1, DESC_SIZE[self.descriptor_type])


# Dynamically adds to the global values the `instances` of the `Hollywood`
# class, instantiated with arguments from `experiments`.
for experiment, setting in experiments.iteritems():
    globals()[experiment] = Hollywood2(**setting)
# A less fancy way to achieve the same result would be:
#    small = Hollywood2(experiments['small'])
#    big = Hollywood2(experiments['big'])


class MySVC(svm.SVC):
    def predict(self, X):
        return self.decision_function(X)


class Classifier:
    def fit(self, tr_kernel, tr_labels):
        """ Fits one-vs-rest classifiers for each class.

        Parameters
        ----------
        tr_kernel: array [n_samples, n_samples]
            Precomputed kernel matrix of the training data.

        tr_labels: list of tuples
            Labels array. Each tuple may contain one or multiple labels.
            Hollywood2 is a multi-label dataset.
        """
        # Convert labels to a label matrix. Element tr_labels[i, j] is 1 if
        # sample `i` belongs to class `j`; otherwise it is -1.
        self.lb = LabelBinarizer(pos_label=1, neg_label=-1)
        tr_labels = self.lb.fit_transform(tr_labels)

        self.nr_classes = tr_labels.shape[1]
        self.clf = []

        for ii in xrange(self.nr_classes):
            class_labels = tr_labels[:, ii]

            my_svm = MySVC(kernel='precomputed', class_weight='auto')

            tuned_parameters = {
                'C': np.power(3.0, np.arange(-2, 8)),
                # 'class_weight': [{-1: 1, 1: 2 ** jj} for jj in xrange(10)]},
            }

            splits = StratifiedShuffleSplit(
                class_labels, 5, test_size=0.25, random_state=1)
            clf_ = GridSearchCV(
                my_svm, tuned_parameters, score_func=average_precision,
                cv=splits, n_jobs=4)
            clf_.fit(tr_kernel, class_labels)
            self.clf.append(clf_.best_estimator_)
        return self

    def score(self, te_kernel, te_labels):
        """ Returns the mean average precision score. """
        te_labels = self.lb.transform(te_labels)
        average_precisions = np.zeros(self.nr_classes)
        preds = []
        for ii in xrange(self.nr_classes):
            class_labels = te_labels[:, ii]
            predicted_values = self.clf[ii].decision_function(te_kernel)
            predicted_values = predicted_values.squeeze()
            preds.append(predicted_values)
            average_precisions[ii] = average_precision(
                class_labels, predicted_values)
        return average_precisions

